import 'package:flutter/material.dart';
import './control.dart';

class Output extends StatefulWidget{
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output>{
  String msg = 'Kris Mulyani';

  void _changeText() {
    setState(() {
      if (msg.startsWith('K')) {
        msg = 'Novia Kris';
      } else if (msg.startsWith('M')) {
        msg = 'Kris Mulyani';
      }else{
        msg = 'Mulyani';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Nama : ', style: new TextStyle(fontSize:30.0),),
          Control(msg),
          RaisedButton(child: Text("Change Name",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}