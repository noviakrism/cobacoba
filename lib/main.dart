import 'package:flutter/material.dart';
import './output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TUGAS UTS',
      home: Scaffold(
        backgroundColor: Colors.orange[100],
        appBar: AppBar(
          backgroundColor: Colors.orange,
          leading: new Icon(Icons.home),
          title: Text('TUGAS UTS'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: Output(),
      ),
    );
  }
}